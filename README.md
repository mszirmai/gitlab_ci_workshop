# workshop-gitlab-ci

## Demo Database

You can create your own database here: elephantsql.com for FREE.

- Create .env file.

```text
DB_URI=postgres://xctzbpus:tZpfHrhkDV65pEDckE9679IBmVOsSr4y@tai.db.elephantsql.com:5432/xctzbpus
```

## How to run CI/CD

### Step 1: Create .gitlab-ci.yml

You can use the built in WebIDE of Gitlab.com to edit files in the repository.
Add a `.gitlab-ci.yml` file into the root of the repository.

[Check the docs about .gitlab-ci.yml file.](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)

[What is yaml](https://www.cloudbees.com/blog/yaml-tutorial-everything-you-need-get-started/)

[How to create a .gitlab-ci.yml file](https://docs.gitlab.com/ee/ci/quick_start/#create-a-gitlab-ciyml-file)

### Step 2: Create jobs

[See documentation about Jobs](https://docs.gitlab.com/ee/ci/jobs/)

```yml
test:
  script:
    - echo "Running testing script"

build:docker:
  script:
    - echo "Running build docker image script"
    - echo "Running push docker image to registry script"

deploy:prod:
  script:
    - echo "Running deploy script"
```

[See documentation about Runners](https://docs.gitlab.com/ee/ci/runners/README.html)

Ensore you have runners available: [Check the docs](https://docs.gitlab.com/ee/ci/quick_start/#ensure-you-have-runners-available)

### Step 3: Add stage to each jobs

[See documentation about Pipelines](https://docs.gitlab.com/ee/ci/pipelines/)

```yml
test:
  stage: test
  script:
    - echo "Running testing script"

build:docker:
  stage: build
  script:
    - echo "Running build docker image script"
    - echo "Running push docker image to registry script"

deploy:prod:
  stage: deploy
  script:
    - echo "Running deploy script"
```

### Step 4: Add stages management to control order of execution and more

[See documentation about stages keyword](https://docs.gitlab.com/ee/ci/yaml/#stages)

```yml
stages:
  - test
  - build
  - deploy

test:
  stage: test
  script:
    - echo "Running testing script"

build:docker:
  stage: build
  script:
    - echo "Running build docker image script"
    - echo "Running push docker image to registry script"

deploy:prod:
  stage: deploy
  script:
    - echo "Running deploy script"
```

### Step 5: Run unit test
[About executors](https://docs.gitlab.com/runner/executors/README.html)

[Docker executor docs](https://docs.gitlab.com/runner/executors/docker.html)

```yml
stages:
  - test
  - build
  - deploy

cache:
  paths:
    - node_modules/

test:
  image: node:10-alpine
  stage: test
  before_script:
    - npm install
  script:
    - npm run test:unit

build:docker:
  stage: build
  script:
    - echo "Running build docker image script"
    - echo "Running push docker image to registry script"

deploy:prod:
  stage: deploy
  script:
    - echo "Running deploy script"
```

### Step 6: Run integration test

[It is a good idea to cache dependencies between jobs. See more here](https://docs.gitlab.com/ee/ci/caching/)

[Caching best practices](https://docs.gitlab.com/ee/ci/caching/index.html#good-caching-practices)

```yml
stages:
  - test
  - build
  - deploy

cache:
  paths:
    - node_modules/

test:
  image: node:10-alpine
  stage: test
  before_script:
    - npm install
  script:
    - npm run test:unit

test:integration:
  image: node:10-alpine
  stage: test
  before_script:
    - npm install
  script:
    - npm run test:integration

build:docker:
  stage: build
  script:
    - echo "Running build docker image script"
    - echo "Running push docker image to registry script"

deploy:prod:
  stage: deploy
  script:
    - echo "Running deploy script"
```

### Step 7: Separate test pipelines

```yml
stages:
  - test
  - test:integration
  - build
  - deploy

cache:
  paths:
    - node_modules/

test:
  image: node:10-alpine
  stage: test
  before_script:
    - npm install
  script:
    - npm run test:unit

test:integration:
  image: node:10-alpine
  stage: test:integration
  before_script:
    - npm install
  script:
    - npm run test:integration

build:docker:
  stage: build
  script:
    - echo "Running build docker image script"
    - echo "Running push docker image to registry script"

deploy:prod:
  stage: deploy
  script:
    - echo "Running deploy script"
```

### Step 8: Test all in one job

[You can also consider using builtin database support. Docs here.](https://docs.gitlab.com/ee/ci/services/postgres.html#use-postgresql-with-the-docker-executor)

[See documentation about cache](https://docs.gitlab.com/ee/ci/caching/)

```yml
stages:
  - test
  - build
  - deploy

cache:
  paths:
    - node_modules/

test:
  image: node:10-alpine
  stage: test
  before_script:
    - npm install
  script:
    - npm run test

build:docker:
  stage: build
  script:
    - echo "Running build docker image script"
    - echo "Running push docker image to registry script"

deploy:prod:
  stage: deploy
  script:
    - echo "Running deploy script"
```

### Step 9: Coverage test and capture it

[See documentation about job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html)

[See documentation about pipeline artifacts](https://docs.gitlab.com/ee/ci/pipelines/pipeline_artifacts.html)

[See documentation of test reporting here](https://docs.gitlab.com/ee/ci/unit_test_reports.html#javascript-example)

```yml
stages:
  - test
  - build
  - deploy

cache:
  paths:
    - node_modules/

test:
  image: node:10-alpine
  stage: test
  before_script:
    - npm install
  script:
    - npm run test:coverage
  coverage: '/^All files\s+\|\s+\d+\.*\d*\s+\|\s*(\d+\.*\d*)/'
  artifacts:
    paths:
      - coverage/

build:docker:
  stage: build
  script:
    - echo "Running build docker image script"
    - echo "Running push docker image to registry script"

deploy:prod:
  stage: deploy
  script:
    - echo "Running deploy script"
```

### Step 10: Build to docker image and push to gitlab registry

[See documentation about available registries](https://docs.gitlab.com/ee/user/packages/)

[Sepcifically about container registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html)

```yml
stages:
  - test
  - build
  - deploy

cache:
  paths:
    - node_modules/

test:
  image: node:10-alpine
  stage: test
  before_script:
    - npm install
  script:
    - npm run test:coverage
  coverage: '/^All files\s+\|\s+\d+\.*\d*\s+\|\s*(\d+\.*\d*)/'
  artifacts:
    paths:
      - coverage/

build:docker:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    - docker push "$CI_REGISTRY_IMAGE"
  only:
    - master

deploy:prod:
  stage: deploy
  script:
    - echo "Running deploy script"
```

### Step 11: Deploy to server
We will use EC2 instances. See excel sheet distrubuted for more details.

- Create `SSH_PRIVATE_KEY`, `SSH_HOSTKEYS` variables (Settings > CI/CD > Vaiables)

- Update `.gitlab-ci.yml`

```yml
stages:
  - test
  - build
  - deploy

cache:
  paths:
    - node_modules/

test:
  image: node:10-alpine
  stage: test
  before_script:
    - npm install
  script:
    - npm run test:coverage
  coverage: '/^All files\s+\|\s+\d+\.*\d*\s+\|\s*(\d+\.*\d*)/'
  artifacts:
    paths:
      - coverage/

build:docker:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    - docker push "$CI_REGISTRY_IMAGE"
  only:
    - master

deploy:prod:
  stage: deploy
  image: alpine
  before_script:
    - apk add --update openssh-client
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | ssh-add -
    - mkdir -p ~/.ssh
    - echo "$SSH_HOSTKEYS" > ~/.ssh/known_hosts
  script:
    - scp -o stricthostkeychecking=no -r ./docker-compose.yml $SSH_USER_SERVER:docker-compose.yml
    - scp -o stricthostkeychecking=no -r ./sql $SSH_USER_SERVER:./sql
    - ssh $SSH_USER_SERVER ls
    - ssh $SSH_USER_SERVER docker-compose down
    - ssh $SSH_USER_SERVER docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - ssh $SSH_USER_SERVER docker pull registry.gitlab.com/somprasongd/gitlab-ci-cd-demo
    - ssh $SSH_USER_SERVER docker-compose up -d
  only:
    - tags
```

### Step12: Merge requests

### Step13: Demo: Install Gitlab Runner
I will demonstrate Gitlab runner installation and configuration. We will try to register the runner for your projects as well if we can.
Also we will try to install different runnerts say Windows and Linux, and will tweek our scrips to run on either of them.

[See documentation](https://docs.gitlab.com/runner/)

### Step14: Demo: Gitlab runner EC2
We will try to use Gitlabs template for AWS EC2 or AWS Fargate service and let Gitlab manage our runners.

[See EC2 runner docks](https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/#autoscaling-gitlab-runner-on-aws-ec2)

[See AWS Farget runner docs](https://docs.gitlab.com/runner/configuration/runner_autoscale_aws_fargate/)

# Additional things to consider
[CI/CD Examples in Gitlab documentation](https://docs.gitlab.com/ee/ci/examples/README.html)
[Templates](https://docs.gitlab.com/ee/ci/examples/README.html#cicd-templates)
